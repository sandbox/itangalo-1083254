<?php
/**
 * @file
 * Contains the validator to replace a nid with nids for referenced nodes.
 */

/**
 * Defines the validator to replace a nid with nids for referenced nodes.
 *
 * @ingroup views_argument_validate_plugins
 */
class viewsrefnid_plugin_argument_validate_nidreplace extends views_plugin_argument_validate {
  var $option_name = 'reference_field';

  // Return an array to use when selecting node reference fields.
  function options() {
    $options = array();
    foreach (content_fields() as $field) {
      if ($field['type'] == 'nodereference') {
        $options[$field['field_name']] = $field['field_name'];
      }
    }
    return $options;
  }

  // The configuration form for this validator. Note the #process and
  // #dependency stuff, showing these settings only for this validator option.
  function validate_form(&$form, &$form_state) {
    $form[$this->option_name] = array(
      '#type' => 'select',
      '#options' => $this->options(),
      '#title' => t('The node reference to use'),
      '#default_value' => $this->get_argument(),
      '#description' => t('The node reference field used to replace the inputted nid.'),
      '#process' => array('expand_checkboxes', 'views_process_dependency'),
      '#dependency' => array('edit-options-validate-type' => array($this->id)),
    );
  }

  // The actual validator, changing the argument values.
  function validate_argument($argument) {
    // set up variables to make it easier to reference during the argument.
    $view = &$this->view;
    $handler = &$this->argument;

    // Load the node provided in the argument.
    $node = node_load($handler->argument);
    // Fail validation if the node couldn't be loaded, or if the node doesn't
    // have the required node reference field.
    if ($node === FALSE || is_null($node->{$this->argument->options[$this->option_name]})) {
      return FALSE;
    }

    // Build a list of all the referenced nids, and replace the original argument.
    $nids = array();
    foreach ($node->{$this->argument->options[$this->option_name]} as $reference) {
      $nids[] = $reference['nid'];
    }
    $handler->argument = implode('+', $nids);

    // If we got this far, approve validation and go home.
    return TRUE;
  }
}
