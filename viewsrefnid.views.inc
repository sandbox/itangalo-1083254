<?php
/**
 * @file
 * Declares the Views plugin for replacing a nid with referenced nids.
 */

function viewsrefnid_views_plugins() {
  return array(
    'argument validator' => array(
      'nidreplace' => array(
        'title' => t('Replace nid with referenced nodes'),
        'handler' => 'viewsrefnid_plugin_argument_validate_nidreplace',
        'path' => drupal_get_path('module', 'viewsrefnid'),
      ),
    ),
  );
}
