This module allows you to use a Views argument validator to transform an
argument from a single nid to a list of nids separated by plus signs, where the
list of nids is fetched following node references on the original node.

So, when is this useful?

If you just want to display information in Views, then you can just skip this
module and use standard relationships in Views instead.

The thing is if you don't only want to view data, but also act on it in one way
or another. In many cases, actions are performed on the *base objects* in the
view, meaning the node that your node references are attached to. One example of
this is Views Bulk Operations, which by standard assumes that you want to act on
the base object – and if you really want to act on the referenced nodes you have
to do a bit of vodoo magic to make it work.

With this module you can load a node using a nid argument, and then replace the
nid with all the referenced nodes, making the argument be 2+3+4+5 instead of the
original nid. If you check the "Allow multiple terms per argument", your view
will end up listing all the referenced nodes *as base objects*. Yeah!

Another Views module acting on base objects is DraggableViews, but those are the
only two examples I know if.

I hope you find it useful.


PS: In a perfect world, this module wouldn't be necessary since the arguments
going in to Views should really be assembled *outside* views – and you could
just use tokens or whatever to get the 2+3+4+5 argumen string instead of the
original nid. But so far, the world isn't perfect.